package tr.com.solveka.jira.plugin.aaug.rest;


import com.atlassian.fugue.Either;
import com.atlassian.jira.bc.user.search.UserSearchParams;
import com.atlassian.jira.bc.user.search.UserSearchService;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserFilter;
import com.atlassian.sal.api.user.UserManager;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import tr.com.solveka.jira.plugin.aaug.rest.response.HelloWorldUserResponse;


import javax.annotation.Nullable;
import javax.ws.rs.*;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/users")
@Consumes({"application/json"})
@Produces({"application/json"})
public class HelloWorldResource {
    private final UserSearchService userSearchService;

    public HelloWorldResource(UserSearchService userSearchService) {
        this.userSearchService = userSearchService;
    }

    @GET
    @Path("all")
    public Response getUsers(@QueryParam("belongingGroup") String belongingGroup) {
        UserSearchParams userSearchParams;

        UserSearchParams.Builder builder = (new UserSearchParams.Builder()).allowEmptyQuery(true).includeActive(true).includeInactive(true).maxResults(100);
        if (StringUtils.isEmpty(belongingGroup)) {
            userSearchParams = builder.build();
        } else {
            UserFilter userFilter = new UserFilter(true, null, Lists.newArrayList(belongingGroup));
            userSearchParams = builder.filter(userFilter).build();
        }
        List<ApplicationUser> users = this.userSearchService.findUsers("", userSearchParams);
        Iterable<HelloWorldUserResponse> responses = Iterables.transform(users, new Function<ApplicationUser, HelloWorldUserResponse>() {

            @Nullable
            @Override
            public HelloWorldUserResponse apply(@Nullable ApplicationUser applicationUser) {
                return new HelloWorldUserResponse(applicationUser);
            }
        });
        return Response.ok(responses).cacheControl(this.never()).build();
    }

    private CacheControl never() {
        CacheControl cacheNever = new CacheControl();
        cacheNever.setNoStore(true);
        cacheNever.setNoCache(true);
        return cacheNever;
    }
}
