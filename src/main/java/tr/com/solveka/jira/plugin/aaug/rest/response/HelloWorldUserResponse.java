package tr.com.solveka.jira.plugin.aaug.rest.response;

import com.atlassian.jira.user.ApplicationUser;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class HelloWorldUserResponse {
    @XmlElement
    private String username;
    @XmlElement
    private String displayName;

    public HelloWorldUserResponse() {
    }

    public HelloWorldUserResponse(ApplicationUser applicationUser) {
        this.username = applicationUser.getUsername();
        this.displayName = applicationUser.getDisplayName();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
