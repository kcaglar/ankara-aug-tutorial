package tr.com.solveka.jira.plugin.aaug.contextprovider;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.web.ContextProvider;
import com.google.common.collect.Maps;

import java.util.Map;

public class HelloWorldContextProvider implements ContextProvider {

    private final JiraAuthenticationContext jiraAuthenticationContext;

    public HelloWorldContextProvider(JiraAuthenticationContext jiraAuthenticationContext) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }
    @Override
    public void init(Map<String, String> map) throws PluginParseException {

    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> map) {
        ApplicationUser loggedInUser = this.jiraAuthenticationContext.getLoggedInUser();
        final Map<String, Object> ctx = Maps.newHashMap(map);
        ctx.put("name", loggedInUser.getDisplayName());
        return ctx;
    }
}
