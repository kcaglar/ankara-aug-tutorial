define('DashboardItem/HelloWorld', ['underscore', 'jquery', 'wrm/context-path'], function (_, $, contextPath) {
    var HelloWorldDashboardItem = function (API) {
        this.API = API;
        this.issues = [];
    };
    /**
     * Called to render the view for a fully configured dashboard item.
     *
     * @param context The surrounding <div/> context that this items should render into.
     * @param preferences The user preferences saved for this dashboard item (e.g. filter id, number of results...)
     */
    HelloWorldDashboardItem.prototype.render = function (context, preferences) {
        this.API.showLoadingBar();
        var $element = this.$element = $(context).find("#dynamic-content");
        var self = this;
        this.requestData(preferences).done(function (data) {
            self.API.hideLoadingBar();
            self.users = data;
            if (self.users === undefined || self.users.length  === 0) {
                $element.empty().html(Dashboard.Item.Templates.Empty());
            }
            else {
                $element.empty().html(Dashboard.Item.Templates.UserList({users: self.users}));
            }
            self.API.resize();
            $element.find(".submit").click(function (event) {
                event.preventDefault();
                self.render(context, preferences);
            });
        });

        this.API.once("afterRender", this.API.resize);
    };

    HelloWorldDashboardItem.prototype.requestData = function (preferences) {
        return $.ajax({
            method: "GET",
            url: contextPath() + "/rest/aaug/1/users/all?belongingGroup="+ preferences['belonging-group']
        });
    };

    HelloWorldDashboardItem.prototype.renderEdit = function (context, preferences) {
        var $element = this.$element = $(context).find("#dynamic-content");
        $element.empty().html(Dashboard.Item.Templates.Configuration({'belongingGroup': preferences['belonging-group']}));
        this.API.once("afterRender", this.API.resize);
        var $form = $("form", $element);
        $(".cancel", $form).click(_.bind(function() {
            if(preferences['belonging-group'])
                this.API.closeEdit();
        }, this));

        $form.submit(_.bind(function(event) {
            event.preventDefault();

            var preferences = getPreferencesFromForm($form);
            this.API.savePreferences(preferences);
            this.API.showLoadingBar();
        }, this));
    };

    function getPreferencesFromForm($form) {
        var preferencesArray = $form.serializeArray();
        var preferencesObject = {};

        preferencesArray.forEach(function(element) {
            preferencesObject[element.name] = element.value;
        });

        return preferencesObject;
    }
    return HelloWorldDashboardItem;
});